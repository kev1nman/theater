const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    minifyCSS = require('gulp-minify-css');

gulp.task('sass', () => {
    gulp.src('./sass/*.scss')
        .pipe(sass({
            //outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            versions: ['lats 2 browsers']
        }))
        .pipe(gulp.dest('./css'));
});

gulp.task('default', () => {
    browserSync.init({
        proxy: 'http://localhost/theater'
    });
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch('./css/*.css').on('change', browserSync.reload);
    gulp.watch('./sass/*.scss').on('change', browserSync.reload);
    gulp.watch('./js/*.js').on('change', browserSync.reload);
    gulp.watch('./sass/*.scss', ['sass']);
});